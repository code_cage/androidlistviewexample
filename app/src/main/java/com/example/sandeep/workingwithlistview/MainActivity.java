package com.example.sandeep.workingwithlistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String[] listItems={"apple","ball","cat","dog","elephant","fan","garden","aeroplane","ship","boat","car","bike"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=findViewById(R.id.listView);
        ArrayAdapter listAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,listItems);
        listView.setAdapter(listAdapter);

    }
}
